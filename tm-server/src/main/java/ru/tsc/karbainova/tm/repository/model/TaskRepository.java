package ru.tsc.karbainova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.model.ITaskRepository;
import ru.tsc.karbainova.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear() {
        entityManager
                .createQuery("DELETE FROM TaskDTO e")
                .executeUpdate();
    }

    @Override
    public void clearByUserId(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public Task findById(@Nullable final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDTO e WHERE e.name = :name AND e.userId = :userId",
                        Task.class
                )
                .setParameter("name", name)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@Nullable final String id) {
        Task reference = entityManager.getReference(Task.class, id);
        entityManager.remove(reference);
    }

    @Override
    public void remove(@NotNull final Task entity) {
        Task reference = entityManager.getReference(Task.class, entity.getId());
        entityManager.remove(reference);
    }

    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("SELECT e FROM TaskDTO e", Task.class).getResultList();
    }

    @Override
    public List<Task> findAllByUserId(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public Task findByIdUserId(@Nullable final String userId, @Nullable final String id) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId",
                        Task.class
                )
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public Task findByIndex(@Nullable final String userId, @NotNull final Integer index) {
        return entityManager
                .createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", Task.class)
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.name = :name AND e.userId = :userId")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @Override
    public void removeByIdUserId(@Nullable final String userId, @NotNull final String id) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId AND e.id=:id")
                .setParameter("id", id)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) {
        entityManager
                .createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId")
                .setParameter("userId", userId)
                .setFirstResult(index).setMaxResults(1).executeUpdate();
    }

    @Override
    public int getCount() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e", Long.class)
                .getSingleResult().intValue();
    }

    @Override
    public int getCountByUser(@NotNull final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM TaskDTO e WHERE e.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void bindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE TaskDTO e SET e.projectId = :projectId WHERE e.userId = :userId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void unbindTaskById(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String taskId
    ) {
        entityManager
                .createQuery(
                        "UPDATE TaskDTO e SET e.projectId = NULL WHERE e.userId = :userId AND e.projectId = :projectId AND e.id = :id"
                )
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<Task> findTasksByUserIdProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        return entityManager
                .createQuery(
                        "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.projectId = :projectId",
                        Task.class
                )
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Override
    public void removeTasksByProjectId(@Nullable final String projectId) {
        entityManager
                .createQuery(
                        "DELETE FROM TaskDTO e WHERE e.projectId = :projectId"
                )
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

}
