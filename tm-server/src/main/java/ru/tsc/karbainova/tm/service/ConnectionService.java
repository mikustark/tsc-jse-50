package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;
import ru.tsc.karbainova.tm.dto.ProjectDTO;
import ru.tsc.karbainova.tm.dto.SessionDTO;
import ru.tsc.karbainova.tm.dto.TaskDTO;
import ru.tsc.karbainova.tm.dto.UserDTO;
import org.hibernate.cfg.Environment;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class ConnectionService implements IConnectionService {
    @NonNull
    private final IPropertyService propertyService;

    @NonNull
    private final EntityManagerFactory entityManagerFactory;

    public ConnectionService(@NonNull IPropertyService propertyService) {
        this.propertyService = propertyService;
        entityManagerFactory = factory();
    }

    @NonNull
    @Override
    public EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }

    @NonNull
    private EntityManagerFactory factory() {
        final String driver = propertyService.getJdbcDriver();
        final String user = propertyService.getJdbcUser();
        final String password = propertyService.getJdbcPassword();
        final String url = propertyService.getJdbcUrl();
        final String dialect = propertyService.getDialect();
        final String auto = propertyService.getAuto();
        final String sqlshow = propertyService.getSqlShow();

        @NonNull Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER, driver);
        settings.put(Environment.USER, user);
        settings.put(Environment.PASS, password);
        settings.put(Environment.URL, url);
        settings.put(Environment.DIALECT, dialect);
        settings.put(Environment.HBM2DDL_AUTO, auto);
        settings.put(Environment.SHOW_SQL, sqlshow);
        settings.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getUseSecondLevelCache());
        settings.put(Environment.USE_QUERY_CACHE, propertyService.getUseQueryCache());
        settings.put(Environment.USE_MINIMAL_PUTS, propertyService.getUseMinimalPuts());
        settings.put(Environment.CACHE_REGION_PREFIX, propertyService.getCacheRegionPrefix());
        settings.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getCacheProviderConfig());
        settings.put(Environment.CACHE_REGION_FACTORY, propertyService.getCacheRegionFactory());


        @NonNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        @NonNull final StandardServiceRegistry registry = registryBuilder.build();
        @NonNull final MetadataSources sources = new MetadataSources(registry);

        sources.addAnnotatedClass(ProjectDTO.class);
        sources.addAnnotatedClass(TaskDTO.class);
        sources.addAnnotatedClass(SessionDTO.class);
        sources.addAnnotatedClass(UserDTO.class);

        sources.addAnnotatedClass(ru.tsc.karbainova.tm.model.Project.class);
        sources.addAnnotatedClass(ru.tsc.karbainova.tm.model.Task.class);
        sources.addAnnotatedClass(ru.tsc.karbainova.tm.model.Session.class);
        sources.addAnnotatedClass(ru.tsc.karbainova.tm.model.User.class);

        @NonNull Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }
}
